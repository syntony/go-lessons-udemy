package main

func main() {
	//write file with our deck
	//cards := newDeck()
	//cards.saveToFile("my_cards")

	//read file with our deck
	//cards := newDeckFromFile("my_cards")
	//cards.print()

	cards := newDeck()
	cards.shuffle()
	cards.print()
}
